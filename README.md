```
You have to recode the libc’s printf function.

• You have to manage the following conversions: sSpdDioOuUxXcC
• You must manage %%
• You must manage the flags #0-+ and space
• You must manage the minimum field-width
• You must manage the precision
• You must manage the flags hh, h, l, ll, j, et z.
```